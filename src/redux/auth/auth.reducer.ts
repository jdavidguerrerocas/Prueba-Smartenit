
import { AuthState, authInitialState } from './auth.state';
import { State } from '@ngrx/store';
import { AllActions, LOGIN_COMPLETED, LOGOUT_COMPLETED, AUTHENTICATED_ERROR, TOKEN_EXPIRED } from './auth.actions';


export function authReducer(state= authInitialState, action: AllActions ): AuthState {
    switch (action.type) {
        case LOGIN_COMPLETED: {
            return  Object.assign({}, state, {
                User: action.payload.user,
                isLoggedIn: action.payload.user != null,
                error: null
            });
        }
        case LOGOUT_COMPLETED: {
        return Object.assign({}, state, {
                User: null,
                isLoggedIn: false,
                error: null
        });
        }
        case AUTHENTICATED_ERROR: {
            return Object.assign({}, state, {
                error: action.payload.error
            });
        }
        case TOKEN_EXPIRED: {
            return  Object.assign({}, state, {
                User: action.payload.user,
                isLoggedIn: action.payload.user != null,
                error: action.error
            });
        }
        default: { return state; }
}

}

export const getIsUserLoggedIn = (state: AuthState) => state.isLoggedIn;

/*
export const getUserData = (state: AuthState) => state.userData;

export const getError = (state: AuthState) => state.error;
*/

