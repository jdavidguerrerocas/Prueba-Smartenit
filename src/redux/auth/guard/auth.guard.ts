import { AppState } from '../../app.state';

import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,  Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import * as states from '../../app.reducer';

@Injectable()
export class AuthGuard implements CanActivate {
  public observable: any;
  constructor(private store: Store<AppState>,
              private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
/*
   if (this.store.select(states.getAuthIsLoggedIn)) {
      console.log(this.store.select(states.getAuthIsLoggedIn));
      return true;
   } else {
    this.router.navigate(['login']);
    return false;
   }

  */
  if (localStorage.getItem('access_token') === null) {
    this.router.navigate(['']);
    return false;
 } else {
   return true;
 }

}

}


