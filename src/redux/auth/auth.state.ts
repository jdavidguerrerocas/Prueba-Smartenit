import { User } from './user.model';




export interface AuthState {

    User: User;
    isLoggedIn: boolean;
    error: string;
}

export const authInitialState: AuthState = {
    User: null,
    isLoggedIn: false,
    error: null,
};



