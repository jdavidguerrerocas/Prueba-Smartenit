import { Router } from '@angular/router';
import { Action } from '@ngrx/store';
import { AuthService} from './auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/debounceTime';
 import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import { Injectable } from '@angular/core';
import { Actions, Effect  } from '@ngrx/effects';

import * as auth from './auth.actions';



@Injectable()
export class AuthEffects {
    constructor(
        private authService: AuthService,
        private actions$:  Actions,
        private router: Router
    ) {}

    @Effect()
    loginAction$: Observable<Action> = this.actions$
    .ofType(auth.LOGIN_REQUESTED)
    .map((action: auth.LoginRequestedAction) => action.payload)
    .switchMap( payload => this.authService.obtainAccessToken(payload.user)
    .map(res => (new auth.LoginCompletedAction(new auth.AuthUserPayload(res))))
    .do(() => this.router.navigate(['/home']))
    .catch((error) => Observable.of(new auth.AuthenticadedErrorAction({error: error})))
    );





@Effect()
logoutAction$: Observable<Action> = this.actions$
    .ofType(auth.LOGOUT_REQUESTED)
    .map((action: auth.LoginRequestedAction) => action.payload)
    .switchMap(payload => this.authService.logout(payload)
        .map(res => (new auth.LogoutCompletedAction(res)))
        .do(() => this.router.navigate(['/']))
        .catch((error) => Observable.of(new auth.AuthenticadedErrorAction({error: error})))
);


}


