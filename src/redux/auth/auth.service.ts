import { User } from './user.model';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';




@Injectable()
export class AuthService {
    token: string;
    constructor(
        private http: HttpClient,
        private router: Router
    ) {

    }

    obtainAccessToken(user): Observable<User> {

        const params = new HttpParams()
        .set('grant_type', 'password')
        .set('username', user.user)
        .set('password', user.password)
        .set('client_secret', 'afnJPwBNtQIx90u2VyHHYVmSadbMsR4uIdbMXGWtk3mt')
        .set('client_id', 'iV1gkyvgQ3MsS9TgHNW6oe');

        const headers = new HttpHeaders().set('Content-type', 'application/x-www-form-urlencoded');

        this.http.post(' https://api.smartenit.io:443/v2/oauth2/token', params, {headers: headers})
        .subscribe(
            data =>  {
              this.saveToken(data); },
            error => {
            }
          );
          return Observable.of(user);
          }

      saveToken(token) {
        this.token = token.access_token.toString();
        localStorage.setItem('access_token', this.token);
        localStorage.setItem('token_type', token.token_type);
        localStorage.setItem('expires_in', token.expires_in );
        console.log(localStorage.getItem('access_token'));
      }

      checkCredentials() {
      }
      logout(user): any {
        localStorage.clear();
        return Observable.of(user);
        // this.router.navigate(['/login']);
      }
}

