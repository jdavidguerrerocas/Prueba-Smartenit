import { Action } from '@ngrx/store';
import { User } from './user.model';
// import { Auth } from './auth.model';

export const LOGIN_REQUESTED = '[auth] Login Requested ';
export const LOGIN_COMPLETED = '[auth] Login Completed ';

export const LOGOUT_REQUESTED = '[auth] Logout Requested ';
export const LOGOUT_COMPLETED = '[auth] Logout Completed ';


export const AUTHENTICATED_ERROR = '[auth] Authenticated Error ';
export const TOKEN_EXPIRED = '[auth] Token Expired';


export class AuthUserPayload {
    constructor( public user: any
            ) { }
}

export class LoginRequestedAction implements Action {
    readonly type = LOGIN_REQUESTED;

    constructor(
        public payload: any
    ) {}

}

export class LoginCompletedAction implements Action {
    readonly type = LOGIN_COMPLETED;

    constructor(
        public payload: AuthUserPayload
    ) {}

}

export class LogoutRequestedAction implements Action {
    readonly type = LOGOUT_REQUESTED;

    constructor(
        public payload= null
    ) {}

}

export class LogoutCompletedAction implements Action {
    readonly type = LOGOUT_COMPLETED;

    constructor(
        public payload= null
    ) {}

}

export class AuthenticadedErrorAction implements Action {
    readonly type = AUTHENTICATED_ERROR;

    constructor(
        public payload: any
    ) {}

}

export class TokenExpiredAction implements Action {
    readonly type = TOKEN_EXPIRED;

    constructor(
        public payload: AuthUserPayload,
        public error: any
    ) {}

}


export type AllActions = LoginRequestedAction |
                         LoginCompletedAction |
                         LogoutRequestedAction |
                         LogoutCompletedAction |
                         AuthenticadedErrorAction |
                         TokenExpiredAction;
