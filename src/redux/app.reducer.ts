import { ActionReducerMap , ActionReducer, MetaReducer} from '@ngrx/store';
import { AppState } from './app.state';
import { createSelector } from 'reselect';
import { authReducer, getIsUserLoggedIn } from './auth/auth.reducer';
import { getDeviceReducer, DeviceReducer} from './devices/device.reducer';
// import { localStorageSync } from 'ngrx-store-localstorage';

export const AppReducer: ActionReducerMap< AppState> = {

    auth: authReducer,
    getDevice: getDeviceReducer,
    devices: DeviceReducer

};


/*export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return localStorageSync({keys: ['auth', 'getDevice']})( AppReducer);
  }
export  const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];
*/
export const getAuthState = (state: AppState) => state.auth;




// export const getAuthUserData = createSelector(getAuthState, fromAuth.getUserData);
export const getAuthIsLoggedIn = createSelector(getAuthState, getIsUserLoggedIn);
// export const getAuthError = createSelector(getAuthState, fromAuth.getError);



