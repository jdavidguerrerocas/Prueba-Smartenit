import { Action } from '@ngrx/store';
import { GetDevice, Device } from './device.model';


export const GET_DEVICE = 'Get Device';
export const REQUEST_DEVICES = 'Request Device';
export const ADD_DEVICES = 'Add Device';

export class GetDeviceAction implements Action {
    readonly type = GET_DEVICE;

    constructor(
        public getDevice: GetDevice
    ) {}

}

export class RequestDeviceAction implements Action {
        readonly type = REQUEST_DEVICES;
        constructor(
            public getDevice: GetDevice
        ) {}


}

export class AddDeviceAction implements Action {
    readonly type = ADD_DEVICES;
    constructor(
        public device: Device[]
    ) {}


}

export type AllActions = GetDeviceAction |
                         RequestDeviceAction |
                         AddDeviceAction;

