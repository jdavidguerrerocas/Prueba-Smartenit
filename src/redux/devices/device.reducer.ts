import { GetDevice, Device } from './device.model';
import { AllActions, GET_DEVICE, ADD_DEVICES } from './device.actions';

const initialState: GetDevice = {

    limit: null,
    fields: null,
    page: null,

};
const initialStateDevice: Device = {
    name: null,
    model: null,
    image: null,
    state: null
};



export function getDeviceReducer(state: GetDevice = initialState, action: AllActions): GetDevice {
    if (action == null ) {
        return state;
    }
    switch (action.type) {
        case GET_DEVICE:
            return action.getDevice;
        default:
            return state;
    }

}


export function DeviceReducer( state: Device[] = [], action: AllActions): Device[] {

    if (action === null) {
        return state;
    }
    switch (action.type) {
        case ADD_DEVICES : {
            return action.device;
        }
        default: {
            return state;
        }
    }

}



