import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GetDevice } from './device.model';

@Injectable()
export class DeviceService {
    token = localStorage.getItem('access_token');
    constructor(
        private http: HttpClient,
        private router: Router
    ) {

    }

    getDevices(device: GetDevice) {
    const params = new HttpParams()
    .set('limit', device.limit)
    .set('fields', device.fields.toString())
    .set('page', device.page);

    const headers = new HttpHeaders()
    .set('Content-type', 'application/json')
    .set('Authorization', 'Bearer ' + this.token)
    .set('Cache-Control', 'no-cache');

    return this.http.get(' https://api.smartenit.io:443/v2/devices/', {  headers: headers , params: params } )
    .map(data => data);

    }

    }

