

export interface GetDevice {
    limit: string;
    fields: string[];
    page: string;
}


export interface Device {
    name: string;
    model: string;
    state: string;
    image: string;

}

