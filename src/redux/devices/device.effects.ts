import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';

// import { GET_DEVICE, ADD_DEVICES, GetDeviceAction, RequestDeviceAction } from './device.actions';
import { map, mergeMap } from 'rxjs/operators';
import * as dev from './device.actions';
import { DeviceService } from './device.service';


@Injectable()

    export class GetDeviceEffects {


        constructor(  private actions$: Actions,
                      private deviceService: DeviceService
        ) {}

        @Effect()
        GetDevices$: Observable<Action> = this.actions$
        .ofType(dev.GET_DEVICE)
        .map((action: dev.GetDeviceAction) => {
               return new dev.RequestDeviceAction(action.getDevice);
            });



        @Effect()
        RequestDevices$: Observable<Action> = this.actions$
        .ofType(dev.REQUEST_DEVICES)
        .pipe(
            mergeMap((action: dev.RequestDeviceAction) => {
                return this.deviceService.getDevices(action.getDevice)
                 .pipe(
                 map((response: any) => {
                       return new dev.AddDeviceAction(response.data.map(item => {
                        console.log(item);
                        return item;
                     }));
                 })
                 );
            })
            );



    }




