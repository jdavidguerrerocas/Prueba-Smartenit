import { AuthState } from './auth/auth.state';
import { GetDevice, Device } from './devices/device.model';
import { Store, State } from '@ngrx/store';
// import { RouterState } from '@ngrx/router-store';

export interface AppState {
   auth: AuthState;
   getDevice: GetDevice;
   devices: Device[];
}



