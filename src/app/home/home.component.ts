import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../redux/app.state';
import { GetDeviceAction } from '../../redux/devices/device.actions';
import { GetDevice, Device } from '../../redux/devices/device.model';
import { LogoutRequestedAction } from '../../redux/auth/auth.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  fields: string [] = ['name', 'model', 'state', 'media'];
  limit: string ;
  pages: Array<number> = [ 1, 2, 3, 4, 5];
  page: string ;
  device: GetDevice;

  devices: Device[] = [];
  constructor(
      private store: Store< AppState>
  ) {
     this.setPage('1');

  }



  ngOnInit() {
  }

  setPage(p) {
    this.page = p;
    this.getDevices();

    this.device = { limit: this.limit,
                    fields: this.fields,
                    page: this.page};

      this.store.dispatch( new GetDeviceAction(this.device));
      this.store.select('devices')
      .subscribe((devices) => {
        this.devices = devices;
      });
  }


getDevices() {

    if ( document.body.clientWidth <= 320) {
      this.limit = '1';
    } else if ( document.body.clientWidth <= 768) {
     this.limit = '2';
    } else if ( document.body.clientWidth <= 1024) {
      this.limit = '3';
    }


    }

logout() {
 this.store.dispatch(new LogoutRequestedAction({}));
}
 }





