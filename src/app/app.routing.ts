import { AuthGuard } from './../redux/auth/guard/auth.guard';
import { ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule, CanActivate} from '@angular/router';
import { AppComponent} from './app.component';
import { LoginComponent} from '../app/login/login.component';
import { HomeComponent } from './home/home.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent,  canActivate: [AuthGuard]},
    {path: 'login', component: LoginComponent},
    {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
    {path: '**', component: HomeComponent}
];

export const appRoutingProviders:  any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
