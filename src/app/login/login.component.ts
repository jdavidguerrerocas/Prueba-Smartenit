
import { AppState } from '../../redux/app.state';
import { AuthState} from '../../redux/auth/auth.state';
import { Store, State} from '@ngrx/store';
import { Observable} from 'rxjs/Observable';
import { AuthService} from '../../redux/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as actions from '../../redux/auth/auth.actions';
// import * as states from '../../redux/app.reducer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  form: FormGroup;

  constructor(
    private fb: FormBuilder,
     private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.form = this.fb.group({
      user: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(6)
      ]]
    });
  }


onLogin() {
    this.store.dispatch(new actions.LoginRequestedAction({user: this.form.value}));
}
}
