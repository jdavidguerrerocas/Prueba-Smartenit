import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import {Routes, RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { getAuthState, AppReducer } from '../redux/app.reducer';


import { AuthService} from '../redux/auth/auth.service';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AuthEffects} from '../redux/auth/auth.effects';
// import { localStorageSync } from 'ngrx-store-localstorage';

// Rutas
import { routing, appRoutingProviders } from './app.routing';
import { HomeComponent } from './home/home.component';


// guards
import { AuthGuard } from '../redux/auth/guard/auth.guard';
import { GetDeviceEffects } from '../redux/devices/device.effects';
import { DeviceService } from '../redux/devices/device.service';
import { DeviceComponent } from './device/device.component';



//




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DeviceComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: LoginComponent }
    ]),
    routing,
    StoreModule.forRoot(AppReducer,
                      // {metaReducers}
                      ),
    StoreDevtoolsModule.instrument({
      maxAge: 25 //  Retains last 25 states
    }),
    EffectsModule.forRoot([
      AuthEffects,
      GetDeviceEffects,
    ])
  ],
  providers: [ AuthService,
    AuthGuard,
    DeviceService,


  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
